package com.base.sys.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.sys.dto.UserMenuPermissionDTO;
import com.base.sys.entity.SysRole;
import com.base.sys.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author gjj
 * @since 2018-11-23
 */
public interface SysUserMapper extends BaseMapper<SysUser> {


    /**
     * 获取该用户拥有的菜单和权限
     * @param id
     * @return
     */
    UserMenuPermissionDTO getUserMenuPermission(@Param("id")String id);


    Page<SysUser> selectPageNew(Page page, @Param("ew") Wrapper wrapper);

    List<SysUser> selectPageNew(@Param("ew") Wrapper wrapper);

}
