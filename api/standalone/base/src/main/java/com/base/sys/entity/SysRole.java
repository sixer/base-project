package com.base.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import org.apache.commons.beanutils.ConvertUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author gjj
 * @since 2018-11-27
 */
@TableName("SYS_ROLE")
public class SysRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("ID")
    private String id;

    /**
     * 角色名称
     */
    @TableField("ROLE_NAME")
    private String roleName;

    /**
     * 创建时间
     */
    @TableField("CREATE_DATE")
    private Date createDate;

    /**
     * 修改时间
     */
    @TableField("UPDATE_DATE")
    private Date updateDate;

    /**
     * 版本
     */
    @TableField("VERSION")
    @Version
    private Integer version;


    @TableField(exist = false)
    private Long[] permIds=new Long[0];
    @TableField(exist = false)
    private String permIdStr;
    @TableField(exist = false)
    private Long[] pPermIds=new Long[0];
    @TableField(exist = false)
    private String pPermIdStr;

    public Long[] getpPermIds() {
        return pPermIds;
    }

    public void setpPermIds(Long[] pPermIds) {
        this.pPermIds = pPermIds;
    }

    public String getpPermIdStr() {
        return pPermIdStr;
    }

    public void setpPermIdStr(String pPermIdStr) {
        this.pPermIdStr = pPermIdStr;
        if(!StringUtils.isEmpty(pPermIdStr)){
            String[] split = pPermIdStr.split(",");
            setpPermIds((Long[]) ConvertUtils.convert(split,Long.class));
        }
    }

    public String getPermIdStr() {
        return permIdStr;
    }

    public void setPermIdStr(String permIdStr) {
        this.permIdStr = permIdStr;
        if(!StringUtils.isEmpty(permIdStr)){
            String[] split = permIdStr.split(",");
            setPermIds((Long[])ConvertUtils.convert(split,Long.class));
        }
    }

    public Long[] getPermIds() {
        return permIds;
    }

    public void setPermIds(Long[] permIds) {
        this.permIds = permIds;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "SysRole{" +
        "id=" + id +
        ", roleName=" + roleName +
        ", createDate=" + createDate +
        ", updateDate=" + updateDate +
        ", version=" + version +
        "}";
    }
}
