package com.base.util;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.base.sys.entity.SysDict;
import com.base.sys.mapper.SysDictMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 对象比较器
 * @author gjj
 */
@Component
public class ObjectCompareUtil {


    @Autowired
    private SysDictMapper sysDictMapper;

    /**
     * 对象比较方法
     * 返回前后修改的记录
     * 需要使用ObjectField注解去标注对象的每个属性
     * 参考UserDO对象
     * @param oldObj
     * @param newObj
     * @return
     */
    public  String compare(final Object oldObj,final Object newObj){
        final StringBuffer sb=new StringBuffer();
        if(oldObj!=null&&newObj!=null){
            Class<?> oldClass = oldObj.getClass();
            Class<?> newClass = newObj.getClass();
            if(oldClass == newClass){
                Field[] fields = oldClass.getDeclaredFields();
                Arrays.stream(fields).forEach(field -> {
                    try{
                        ObjectField objectField = field.getAnnotation(ObjectField.class);
                        if(objectField!=null){
                            String name = objectField.value();
                            String dicType = objectField.dicType();
                            field.setAccessible(true);
                            List<SysDict> sysDicts=new ArrayList<SysDict>();
                            if(!StringUtils.isEmpty(dicType)){
                                QueryWrapper<SysDict> queryWrapper=new QueryWrapper<SysDict>();
                                queryWrapper.eq("type",dicType);
                                sysDicts = sysDictMapper.selectList(queryWrapper);
                            }
                            Object oldValue = field.get(oldObj);
                            Object newValue = field.get(newObj);
                            Class valueClass = getValueClass(oldValue, newValue);
                            Date date = new Date();
                            if (valueClass == date.getClass()) {
                                oldValue = oldValue==null ? null : DateUtils.format((Date) oldValue, DateUtils.DATE_TIME_PATTERN);
                                newValue = newValue==null ? null : DateUtils.format((Date) newValue, DateUtils.DATE_TIME_PATTERN);
                            } else {
                                if(!StringUtils.isEmpty(dicType)){
                                    final Object old=oldValue;
                                    SysDict one = sysDicts.stream().filter(sysDict -> {
                                        return sysDict.getValue()==old;
                                    }).findFirst().get();
                                    oldValue=one.getName();

                                    final Object newValue1=newValue;
                                    one = sysDicts.stream().filter(sysDict -> {
                                        return sysDict.getValue()==newValue1;
                                    }).findFirst().get();
                                    newValue=one.getName();
                                }

                                oldValue = oldValue==null ? null : String.valueOf(oldValue);
                                newValue = newValue==null ? null : String.valueOf(newValue);
                            }
                            if(oldValue==null && newValue!=null){
                                sb.append(name+"由: ,修改为:"+newValue+";");
                            }
                            else if(oldValue!=null&& newValue==null){
                                sb.append(name+"由:"+oldValue+",修改为: ;");
                            }
                            else if(!oldValue.equals(newValue)){
                                String s = oldValue.toString();
                                if(s.contains("files")){
                                    //这里的project  在前端显示时需要去替换对应的项目名
                                    sb.append(name+"由:<img width='50px' height='50px' src='/project/"+oldValue+"'>,修改为:<img width='50px' height='50px' src='/project/"+newValue+"'>;");
                                }else{
                                    sb.append(name+"由:"+oldValue+",修改为:"+newValue+";");
                                }
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                });
            }
        }
        return sb.toString();
    }


    private static Class getValueClass(Object oldValue,Object newValue){
        if(oldValue!=null){
            return oldValue.getClass();
        }
        if(newValue!=null){
            return newValue.getClass();
        }
        return null;
    }


}
