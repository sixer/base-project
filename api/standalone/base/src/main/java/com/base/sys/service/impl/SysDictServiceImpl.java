package com.base.sys.service.impl;

import com.base.sys.entity.SysDict;
import com.base.sys.mapper.SysDictMapper;
import com.base.sys.service.ISysDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.base.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author gjj
 * @since 2018-11-30
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements ISysDictService {

    @Autowired
    private SysDictMapper sysDictMapper;

    @Override
    public Result getTables(String tableName){
        List<String> tables = sysDictMapper.getTables("%"+tableName+"%");
        return Result.successResult(tables);
    }

}
