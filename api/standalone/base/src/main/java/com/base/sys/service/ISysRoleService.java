package com.base.sys.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.sys.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;
import com.base.util.Result;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author gjj
 * @since 2018-11-27
 */
public interface ISysRoleService extends IService<SysRole> {


    Page selectPageNew(Page page, Wrapper wrapper);

    /**
     * 批量删除，同时删除角色权限表
     * @param list
     * @return
     */
    Result deleteBatch(List<SysRole> list);

    Result assignPerm(SysRole role);
}
