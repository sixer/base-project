package com.base.sys.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author gjj
 * @since 2018-11-27
 */
@RestController
@RequestMapping("/sys/sys-user-role")
public class SysUserRoleController {

}
