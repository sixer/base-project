package com.base.sys.mapper;

import com.base.sys.entity.SysDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author gjj
 * @since 2018-11-30
 */
public interface SysDictMapper extends BaseMapper<SysDict> {


    List<String> getTables(@Param("tableName") String tableName);

}
