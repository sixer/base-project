package com.base.sys.dto;

import com.base.util.PageQuery;

public class SysUserQueryDTO extends PageQuery {


    private String username;

    private String name;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
