package com.base.sys.mapper;

import com.base.sys.entity.SysUserUpdateLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author gjj
 * @since 2018-12-06
 */
public interface SysUserUpdateLogMapper extends BaseMapper<SysUserUpdateLog> {

}
