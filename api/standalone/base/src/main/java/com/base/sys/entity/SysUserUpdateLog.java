package com.base.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author gjj
 * @since 2018-12-06
 */
@TableName("SYS_USER_UPDATE_LOG")
public class SysUserUpdateLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("ID")
    private String id;

    /**
     * 用户id
     */
    @TableField("USER_ID")
    private String userId;

    /**
     * 操作时间
     */
    @TableField("OPERATE_TIME")
    private Date operateTime;

    /**
     * 操作人 
     */
    @TableField("OPERATE_PERSON")
    private String operatePerson;

    /**
     * 操作内容
     */
    @TableField("OPERATE_CONTENT")
    private String operateContent;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    public Date getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(Date operateTime) {
        this.operateTime = operateTime;
    }

    public String getOperatePerson() {
        return operatePerson;
    }

    public void setOperatePerson(String operatePerson) {
        this.operatePerson = operatePerson;
    }
    public String getOperateContent() {
        return operateContent;
    }

    public void setOperateContent(String operateContent) {
        this.operateContent = operateContent;
    }

    @Override
    public String toString() {
        return "SysUserUpdateLog{" +
        "id=" + id +
        ", userId=" + userId +
        ", operateTime=" + operateTime +
        ", operatePerson=" + operatePerson +
        ", operateContent=" + operateContent +
        "}";
    }
}
